import React from "react";
import PropTypes from "prop-types";
import {
    FormControl,
    FormHelperText,
    InputLabel,
    Input
} from "@material-ui/core";

function InputField({
    id,
    elementStyle,
    label,
    value,
    required,
    error,
    handleChange,
    autoComplete,
    autoFocus,
    type
}) {
    const name = label.toLowerCase();

    return (
        <FormControl error={!!error} required={required} className={elementStyle}>
            <InputLabel htmlFor={id}>{label}</InputLabel>
            <Input
                id={id}
                name={name}
                type={type}
                value={value}
                inputComponent="input"
                onChange={handleChange}
                autoComplete={autoComplete}
                fullWidth
                autoFocus={autoFocus}
            />
            {error ? (
                <FormHelperText htmlFor={id} error={!!error}>
                    {error}
                </FormHelperText>
            ) : null}
        </FormControl>
    );
}

InputField.propTypes = {
    id           : PropTypes.string,
    elementStyle : PropTypes.string,
    error        : PropTypes.string,
    required     : PropTypes.bool,
    autoFocus    : PropTypes.bool,
    value        : PropTypes.string,
    type         : PropTypes.string,
    autoComplete : PropTypes.string,
    label        : PropTypes.string.isRequired,
    handleChange : PropTypes.func.isRequired
};

export default InputField;
