import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import {
    FormControl,
    FormHelperText,
    InputLabel,
    Select as SelectBase,
    Checkbox,
    MenuItem
} from "@material-ui/core";

import { MenuProps } from "./selectMenuProps.style";

function Select({
    id,
    elementStyle,
    multiple,
    label,
    value,
    valueList,
    error,
    required,
    handleChange
}) {
    const [ errored, setErrored ] = useState("");
    const name = label.toLowerCase();

    function renderSelectedValue(selected) {
        return multiple ? selected.join(", ") : selected;
    }
    const renderSelectsList = (list, checkedList) => {
        const forMap = list && list.length ? list : [];

        return forMap.map(elem => (
            <MenuItem key={elem} value={elem}>
                {multiple ? (
                    <Checkbox checked={checkedList.indexOf(elem) > -1} />
                ) : null}
                {elem}
            </MenuItem>
        ));
    };

    useEffect(() => setErrored(!!error), []);

    return (
        <FormControl error={errored} required={required} className={elementStyle}>
            <InputLabel htmlFor={id}>{label}</InputLabel>
            <SelectBase
                multiple={multiple}
                value={value}
                onChange={handleChange}
                inputProps={{ name, id }}
                renderValue={renderSelectedValue}
                MenuProps={MenuProps}
            >
                {renderSelectsList(valueList, value)}
            </SelectBase>
            {error ? (
                <FormHelperText htmlFor={id} error={!!error}>
                    {error}
                </FormHelperText>
            ) : null}
        </FormControl>
    );
}


Select.propTypes = {
    id           : PropTypes.string,
    elementStyle : PropTypes.string,
    required     : PropTypes.bool,
    multiple     : PropTypes.bool,
    error        : PropTypes.string,
    value        : PropTypes.any,
    label        : PropTypes.string.isRequired,
    valueList    : PropTypes.array,
    handleChange : PropTypes.func.isRequired
};

export default Select;
