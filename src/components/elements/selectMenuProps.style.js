const ITEM_HEIGHT = 48;

export const MenuProps = { PaperProps: { style: { maxHeight: ITEM_HEIGHT * 4.5 } } };
