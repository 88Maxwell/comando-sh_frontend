import React from "react";
import PropTypes from "prop-types";
import {
    Dialog as DialogBase,
    DialogTitle,
    Grid,
    DialogContentText,
    DialogContent
} from "@material-ui/core";

function Dialog({ label, onClose, isOpen, children }) {
    return (
        <DialogBase open={isOpen} onClose={onClose} aria-labelledby={`${label}-dialog`}>
            <DialogTitle id="create-wine-dialog">{label}</DialogTitle>
            <DialogContent>
                <Grid container direction="column">
                    <DialogContentText>{children}</DialogContentText>
                </Grid>
            </DialogContent>
        </DialogBase>
    );
}

Dialog.propTypes = {};

export default Dialog;

Dialog.propTypes = {
    label    : PropTypes.string.isRequired,
    children : PropTypes.node.isRequired,
    isOpen   : PropTypes.bool.isRequired,
    onClose  : PropTypes.func.isRequired
};
