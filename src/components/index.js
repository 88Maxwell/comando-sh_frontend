/* eslint-disable no-unused-vars */
import React, { Component } from "react";
import { Switch } from "react-router";
import { Route, Router } from "react-router-dom";

import Controller from "../utils/Controller";

import history from "../utils/history";

import Clear from "./layouts/Clear";
import Main from "./layouts/Main";

import Auth from "./pages/Auth";
import Profile from "./pages/Profile";
import Rooms from "./pages/Rooms";
import Room from "./pages/Room";

export default class App extends Component {
    render() {
        return (
            <Router history={history}>
                <Switch>
                    <Controller
                        path="/"
                        exact
                        Layout={Clear}
                        Page={Auth}
                    />
                    <Controller
                        path="/profile"
                        exact
                        Layout={Main}
                        Page={Profile}
                    />
                    <Controller
                        path="/rooms"
                        exact
                        Layout={Main}
                        Page={Rooms}
                    />
                    <Controller
                        path="/room/:roomId"
                        exact
                        Layout={Main}
                        Page={Room}
                    />
                </Switch>
            </Router>
        );
    }
}

