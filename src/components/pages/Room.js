import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";

import { observer, inject } from "mobx-react";
import { Button, Divider, Grid, Typography } from "@material-ui/core";
import Dialog from "../dialogs/Dialog";
import CreateScriptForm from "../forms/CreateScript";
import CreateCollectionForm from "../forms/CreateCollection";
import ExecutorsList from "../widgets/ExecutorsList";
import Collection from "../widgets/Collection";
import config from "../../../configs";

function Room({ roomsStore, collectionStore, match }) {
    const [ data, setData ] = useState(null);

    const [ scriptDialog, setScriptDialog ] = useState(false);
    const [ collectionDialog, setCollectionDialog ] = useState(false);
    const [ expandedId, setExpandedId ] = useState(false);
    const [ executorPollingId, setExecutorPollingId ] = useState(null);

    const isActive = data?.status === "ACTIVE";

    useEffect(() => {
        const roomId = match.params.roomId;
        const rooms = roomsStore.rooms.find(room => room.id === roomId);

        setData(rooms);
        setExecutorPollingId(getExecutorPollingInterval(rooms));

        collectionStore.fetchCollections(roomId);

        return () => {
            if (executorPollingId) {
                clearInterval(executorPollingId);
                setExecutorPollingId(null);
            }
        };
    }, []);

    function handleExpand(expanded) {
        return () => setExpandedId(expandedId === expanded ? null : expanded);
    }

    function handleChangeRoomStatus() {
        roomsStore.changeStatus(
            data?.id,
            data?.status === "ACTIVE" ? "STOPED" : "ACTIVE"
        );
    }

    function handleDeleteRoom() {
        return roomsStore.deleteRoom(data?.id);
    }
    // eslint-disable-next-line no-unused-vars
    function handleOpenScriptDialog() {
        setScriptDialog(true);
    }
    function handleCloseScriptDialog() {
        setScriptDialog(true);
    }
    function handleOpenCollectionDialog() {
        setCollectionDialog(true);
    }
    function handleCloseCollectionDialog() {
        setCollectionDialog(false);
    }

    function getExecutorPollingInterval() {
        return setInterval(async () => {
            const newData = (await roomsStore.fetchRoom(data?.id)) || {};
            const arrayDiff = newData.Executors.filter(newExecutor =>
                !data?.Executors.find(executor => newExecutor.id === executor.id)
                    ? newExecutor
                    : null
            );

            if (data && arrayDiff !== 0) setData(newData);
        }, config.pollingInterval);
    }

    return (
        <div>
            <Button onClick={handleOpenCollectionDialog}>Create collection</Button>
            <Button onClick={handleChangeRoomStatus}>
                {isActive ? "Stop" : "Activate"}
            </Button>
            <Button onClick={handleDeleteRoom}>Delete room</Button>
            <Divider />
            <Grid container>
                <Grid item xs={9}>
                    {collectionStore?.collections?.length ? (
                        collectionStore.collections.map(collection => (
                            <Collection
                                executors={data?.Executors}
                                key={collection.id}
                                data={collection}
                                expanded={expandedId === collection.id}
                                handleExpand={handleExpand(collection.id)}
                            />
                        ))
                    ) : (
                        <Typography>There no wines</Typography>
                    )}
                </Grid>
                <Grid item xs={3}>
                    <ExecutorsList executors={data?.Executors} />
                </Grid>
            </Grid>
            {collectionDialog ? (
                <Dialog
                    isOpen={collectionDialog}
                    label="Create collection"
                    onClose={handleCloseCollectionDialog}
                >
                    <CreateCollectionForm
                        roomId={data?.id}
                        onClose={handleCloseCollectionDialog}
                    />
                </Dialog>
            ) : null}
            {scriptDialog ? (
                <Dialog
                    isOpen={scriptDialog}
                    label="Create script"
                    onClose={handleCloseScriptDialog}
                >
                    <CreateScriptForm
                        executors={data?.Executors}
                        roomId={data?.id}
                        onClose={handleCloseScriptDialog}
                    />
                </Dialog>
            ) : null}
        </div>
    );
}

Room.propTypes = {
    roomsStore      : PropTypes.object.isRequired,
    collectionStore : PropTypes.object.isRequired,
    match           : PropTypes.object.isRequired
};

export default inject("roomsStore", "collectionStore")(observer(Room));
