import React from "react";
import PropTypes from "prop-types";

import { CssBaseline, Grid } from "@material-ui/core";

import withStyles from "@material-ui/core/styles/withStyles";

import Login from "../widgets/Login";

import { muiStyles } from "./auth.styles";

function Auth({ classes }) {
    return (
        <Grid container className={classes.grid} justify="space-around">
            <CssBaseline />
            <Grid item xs={5} lg={4}>
                <Login type="Sign In" />
            </Grid>
            <Grid item xs={5} lg={4}>
                <Login type="Register" />
            </Grid>
        </Grid>
    );
}

Auth.propTypes = {
    classes : PropTypes.object.isRequired
};

export default withStyles(muiStyles)(Auth);
