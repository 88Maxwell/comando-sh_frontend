export const muiStyles = theme => {
    const {
        spacing,
        palette: { secondary }
    } = theme;

    return {
        grid : {
            padding : 60
        },
        paper : {
            marginTop     : spacing(8),
            display       : "flex",
            flexDirection : "column",
            alignItems    : "center",
            padding       : `${spacing(2)}px ${spacing(3)}px ${spacing(3)}px`
        },
        avatar : {
            margin          : spacing(1),
            backgroundColor : secondary.main
        },
        formControl : {
            width     : "100%", // Fix IE 11 issue.
            marginTop : spacing(1)
        },
        submit : { marginTop: spacing(3) }
    };
};
