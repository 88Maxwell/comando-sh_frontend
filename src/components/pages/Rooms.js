import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { observer, inject } from "mobx-react";
import { Button, Divider, Paper } from "@material-ui/core";
import Dialog from "../dialogs/Dialog";
import CreateRoomForm from "../forms/CreateRoom";
import RoomsList from "../widgets/RoomsList";

function Rooms({ roomsStore }) {
    const [ roomDialog, setRoomDialog ] = useState(false);

    useEffect(() => {
        roomsStore.fetchRooms();
    }, []);

    function handleOpenDialog() {
        setRoomDialog(true);
    }
    function handleCloseDialog() {
        setRoomDialog(false);
    }

    return (
        <div>
            <Button onClick={handleOpenDialog}>
                Create new room
            </Button>
            <Divider />
            <Paper>
                <RoomsList rooms={roomsStore.rooms} />
            </Paper>
            {roomDialog ? (
                <Dialog
                    isOpen={roomDialog}
                    label="Create new room"
                    onClose={handleCloseDialog}
                >
                    <CreateRoomForm onClose={handleCloseDialog} />
                </Dialog>
            ) : null}
        </div>
    );
}

Rooms.propTypes = {
    roomsStore : PropTypes.object.isRequired
};

export default inject("roomsStore")(observer(Rooms));
