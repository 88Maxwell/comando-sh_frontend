import React from "react";
import PropTypes from "prop-types";
import { List, ListItem, Typography, Grid, Button, Divider } from "@material-ui/core";
import { Delete } from "@material-ui/icons";

function ScriptList({ scripts = [] }) {
    return (
        scripts.length ?
            <List>
                {scripts.map((script, index) => (
                    <ListItem
                        button
                        key={script.id}
                    >
                        <div>{index}</div>
                        <Grid container justify="space-between" alignItems="center">
                            <Grid item>
                                <Typography>{script.data}</Typography>
                                <Divider />
                                <Typography>{script.executorId}</Typography>
                                <Typography>{script.executorId}</Typography>
                            </Grid>
                            <Grid item>
                                <Button >
                                    <Typography>{script.status}</Typography>
                                </Button>
                                <Button>
                                    <Delete />
                                </Button>
                            </Grid>
                        </Grid>
                    </ListItem>))}
            </List>
            : <div> Empty list of scripts</div>
    );
}

ScriptList.propTypes = {
    scripts : PropTypes.array

};

export default ScriptList;

