
import React from "react";
import PropTypes from "prop-types";

import { List, ListItem } from "@material-ui/core";
import history from "../../utils/history";

function RoomsList({ rooms = [] }) {
    const handleOpenRoom = roomId => () => history.push(`/room/${roomId}`);

    return (
        rooms.length ?
            <List>
                {rooms.map(room => (
                    <ListItem
                        button
                        key={room.id}
                        onClick={handleOpenRoom(room.id)}
                    >
                        {room.name}
                    </ListItem>))}
            </List>
            : <div> Empty list of rooms</div>
    );
}

RoomsList.propTypes = {
    rooms : PropTypes.array
};

export default RoomsList;

