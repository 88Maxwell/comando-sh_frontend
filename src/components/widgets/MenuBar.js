import React from "react";
import PropTypes from "prop-types";

import classNames from "classnames";
import { Drawer, Divider, IconButton, withStyles } from "@material-ui/core";
import { ChevronLeft as ChevronLeftIcon } from "@material-ui/icons";

import { muiStyles } from "./menuBar.styles";
import MenuList from "./MenuList";

function MenuBar({ classes, open, handleDrawerClose }) {
    return (
        <Drawer
            variant="permanent"
            classes={{
                paper : classNames(
                    classes.drawerPaper,
                    !open && classes.drawerPaperClose
                )
            }}
            open={open}
        >
            <div className={classes.toolbarIcon}>
                <IconButton onClick={handleDrawerClose}>
                    <ChevronLeftIcon />
                </IconButton>
            </div>
            <Divider />
            <MenuList />
        </Drawer>
    );
}

MenuBar.propTypes = {
    classes           : PropTypes.object.isRequired,
    open              : PropTypes.bool.isRequired,
    handleDrawerClose : PropTypes.func.isRequired
};

export default withStyles(muiStyles)(MenuBar);
