import { observer, inject } from "mobx-react";

import { withStyles } from "@material-ui/core/styles";
import {
    Grid,
    Button,
    Divider,
    Typography,
    List,
    ExpansionPanel,
    ExpansionPanelSummary,
    ExpansionPanelDetails
} from "@material-ui/core";
// eslint-disable-next-line no-unused-vars
import { Delete, PlayArrow } from "@material-ui/icons";

import React, { useState } from "react";
import PropTypes from "prop-types";
import Dialog from "../dialogs/Dialog";
import CreateScriptForm from "../forms/CreateScript";
import ScriptList from "./ScriptList";

import { muiStyles } from "./collection.styles";

function Collection({
    classes,
    data,
    expanded,
    handleExpand,
    collectionStore,
    executors
}) {
    const [ scriptDialog, setScriptDialog ] = useState(false);

    // function handleRemove() {
    //     return async function (evt) {
    //         evt.stopPropagation();
    //         evt.preventDefault();
    //         const { data, remove } = this.props;

    //         await remove(data.id);
    //         setScriptDialog(false);
    //     };
    // }

    function handleOpenScriptDialog() {
        setScriptDialog(true);
    }
    function handleCloseScriptDialog() {
        setScriptDialog(false);
    }


    async function handleExecuteCollection() {
        await collectionStore.executeCollection(data.id, data.Scripts);
    }

    const { findById, collections } = collectionStore;
    const { Scripts } = findById(collections, data.id);

    return (
        <ExpansionPanel expanded={expanded} onChange={handleExpand}>
            <ExpansionPanelSummary className={classes.expansionSummary}>
                <Grid container justify="space-between" alignItems="center">
                    <Grid item>
                        <Typography>{data.id}</Typography>
                    </Grid>
                    <Grid item>
                        <Button onClick={handleExecuteCollection}>
                            <PlayArrow />
                        </Button>
                        {/* <Button onClick={this.handleOpenDialog("confirmRemoveDialog")}>
                            <Delete />
                        </Button> */}
                    </Grid>
                </Grid>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails className={classes.expansionDetails}>
                <List component="ul" className={classes.wineDataList}>
                    <ScriptList scripts={Scripts} />
                    <Divider />
                    <div>
                        <Button onClick={handleOpenScriptDialog}>
                            Add script
                        </Button>
                    </div>
                </List>
            </ExpansionPanelDetails>
            {scriptDialog ? (
                <Dialog
                    isOpen={scriptDialog}
                    label="Create script"
                    onClose={handleCloseScriptDialog}
                >
                    <CreateScriptForm
                        executors={executors}
                        collectionId={data.id}
                        onClose={handleCloseScriptDialog}
                    />
                </Dialog>
            ) : null}
        </ExpansionPanel>
    );
}

Collection.propTypes = {
    data            : PropTypes.object.isRequired,
    classes         : PropTypes.object.isRequired,
    handleExpand    : PropTypes.func.isRequired,
    executors       : PropTypes.array.isRequired,
    collectionStore : PropTypes.object.isRequired,
    expanded        : PropTypes.bool.isRequired
};

export default inject("collectionStore")(
    observer(withStyles(muiStyles)(Collection))
);
