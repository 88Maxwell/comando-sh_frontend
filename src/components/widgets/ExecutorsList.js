import React from "react";
import PropTypes from "prop-types";
import { List, ListItem, Divider, Paper } from "@material-ui/core";

const ExecutorsList = ({ executors = [] }) => {
    const connectedExecutors = [];

    executors.forEach(executor => executor.isConnected ? connectedExecutors.push(executor) : null);

    return (
        connectedExecutors.length ?
            <List>
                {connectedExecutors.map(executor => (
                    <Paper key={executor.id}>
                        <List>
                            <ListItem>{executor.name}</ListItem>
                            <Divider />
                        </List>
                    </Paper>

                ))}
            </List>
            : <div> Empty list of connected executors</div>
    );
};

ExecutorsList.propTypes = {
    executors : PropTypes.array
};

export default ExecutorsList;
