import drawerWidth from "./drawerWidth.styles";

const transitionStyle = transitions =>
    transitions.create("width", {
        easing   : transitions.easing.sharp,
        duration : transitions.duration.leavingScreen
    });

export const muiStyles = ({ mixins, transitions, spacing, breakpoints }) => ({
    toolbar     : { paddingRight: 24 }, // keep right padding when drawer closed
    toolbarIcon : {
        display        : "flex",
        alignItems     : "center",
        justifyContent : "flex-end",
        padding        : "0 8px",
        ...mixins.toolbar
    },
    drawerPaper : {
        position   : "relative",
        whiteSpace : "nowrap",
        width      : drawerWidth,
        transition : transitionStyle(transitions)
    },
    drawerPaperClose : {
        overflowX              : "hidden",
        transition             : transitionStyle(transitions),
        width                  : spacing(7),
        [breakpoints.up("sm")] : { width: spacing(9) }
    }
});

