import drawerWidth from "./drawerWidth.styles";

export const muiStyles = ({ zIndex, transitions }) => ({
    toolbar : {
        paddingRight : 24 // keep right padding when drawer closed
    },
    appBar : {
        zIndex     : zIndex.drawer + 1,
        transition : transitions.create([ "width", "margin" ], {
            easing   : transitions.easing.sharp,
            duration : transitions.duration.leavingScreen
        })
    },
    appBarShift : {
        marginLeft : drawerWidth,
        width      : `calc(100% - ${drawerWidth}px)`,
        transition : transitions.create([ "width", "margin" ], {
            easing   : transitions.easing.sharp,
            duration : transitions.duration.enteringScreen
        })
    },
    menuButton       : { marginLeft: 12, marginRight: 36 },
    menuButtonHidden : { display: "none" },
    title            : { flexGrow: 1 }
});
