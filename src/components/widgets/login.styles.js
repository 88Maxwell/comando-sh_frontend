export const muiStyles = theme => {
    const {
        spacing,
        palette: { secondary }
    } = theme;

    return {
        grid : {
            marginLeft  : "auto",
            marginRight : "auto"
        },
        paper : {
            marginTop     : spacing(8),
            display       : "flex",
            flexDirection : "column",
            alignItems    : "center",
            padding       : `${spacing(2)}px ${spacing(3)}px ${spacing(3)}px`
        },
        avatar : {
            margin          : spacing(1),
            backgroundColor : secondary.main
        }
    };
};
