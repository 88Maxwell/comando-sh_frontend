import React from "react";
import PropTypes from "prop-types";
import { Avatar, Paper, Typography } from "@material-ui/core";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";

import withStyles from "@material-ui/core/styles/withStyles";
import SignInForm from "../forms/SignIn";
import Register from "../forms/Register";
import { muiStyles } from "./login.styles";

function Login({ classes, type }) {
    return (
        <Paper className={classes.paper}>
            <Avatar className={classes.avatar}>
                <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
                {type}
            </Typography>
            {type === "Sign In" ? <SignInForm /> : <Register />}
        </Paper>
    );
}

Login.propTypes = {
    classes : PropTypes.object.isRequired,
    type    : PropTypes.string.isRequired
};

export default withStyles(muiStyles)(Login);
