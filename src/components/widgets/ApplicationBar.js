import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { observer, inject } from "mobx-react";

import {
    AppBar,
    Toolbar,
    Typography,
    IconButton,
    withStyles
} from "@material-ui/core";

import {
    Menu as MenuIcon,
    PowerSettingsNew as PowerSettingsNewIcon
} from "@material-ui/icons";

import { muiStyles } from "./applicationBar.styles";

function ApplicationBar({ classes, open, userStore, handleDrawerOpen }) {
    return (
        <AppBar
            position="absolute"
            className={classNames(classes.appBar, open && classes.appBarShift)}
        >
            <Toolbar disableGutters={!open} className={classes.toolbar}>
                <IconButton
                    color="inherit"
                    onClick={handleDrawerOpen}
                    className={classNames(
                        classes.menuButton,
                        open && classes.menuButtonHidden
                    )}
                >
                    <MenuIcon />
                </IconButton>
                <Typography
                    component="h1"
                    variant="h6"
                    color="inherit"
                    noWrap
                    className={classes.title}
                >
                    Comander_sh
                </Typography>
                <IconButton onClick={userStore.logout} color="inherit">
                    <PowerSettingsNewIcon />
                </IconButton>
            </Toolbar>
        </AppBar>
    );
}

ApplicationBar.propTypes = {
    classes          : PropTypes.object.isRequired,
    open             : PropTypes.bool.isRequired,
    handleDrawerOpen : PropTypes.func.isRequired,
    userStore        : PropTypes.object.isRequired
};

export default inject("userStore")(
    observer(withStyles(muiStyles)(ApplicationBar))
);
