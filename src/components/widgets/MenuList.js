import React from "react";

import {
    List,
    ListItem,
    ListItemIcon,
    ListItemText
} from "@material-ui/core";
import { Person as PersonIcon } from "@material-ui/icons";
import { Link } from "react-router-dom";

export default () => (
    <List>
        <ListItem button component={Link} to="/profile">
            <ListItemIcon>
                <PersonIcon />
            </ListItemIcon>
            <ListItemText primary="Profile" />
        </ListItem>
        <ListItem button component={Link} to="/rooms">
            <ListItemIcon>
                <PersonIcon />
            </ListItemIcon>
            <ListItemText primary="Rooms" />
        </ListItem>
    </List>
);
