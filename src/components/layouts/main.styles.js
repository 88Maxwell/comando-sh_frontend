export const muiStyles = ({ spacing, mixins }) => ({
    root         : { display: "flex" },
    appBarSpacer : mixins.toolbar,
    content      : {
        flexGrow : 1,
        padding  : spacing(3),
        height   : "100vh",
        overflow : "auto"
    }
});
