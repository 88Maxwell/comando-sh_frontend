export const muiStyles = theme => {
    const { spacing } = theme;

    return {
        main : {
            width       : "auto",
            display     : "block", // Fix IE 11 issue.
            marginLeft  : spacing(3),
            marginRight : spacing(3)
        }
    };
};
