import React, { useEffect } from "react";
import PropTypes from "prop-types";

import withStyles from "@material-ui/core/styles/withStyles";
import { Grid } from "@material-ui/core";

import history from "../../utils/history";

import { muiStyles } from "./main.styles";

function Clear({ classes, children }) {
    useEffect(() => () => {
        if (localStorage.token) history.push("/profile");
    });

    return (
        <main className={classes.main}>
            <Grid container>{children}</Grid>
        </main>
    );
}

Clear.propTypes = {
    classes  : PropTypes.object.isRequired,
    children : PropTypes.node.isRequired
};

export default withStyles(muiStyles)(Clear);
