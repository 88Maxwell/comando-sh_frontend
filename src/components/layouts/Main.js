import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";

import { observer, inject } from "mobx-react";
import { withStyles } from "@material-ui/core/styles";
import { CssBaseline } from "@material-ui/core";

import MenuBar from "../widgets/MenuBar";
import ApplicationBar from "../widgets/ApplicationBar";

import { muiStyles } from "./main.styles";

function Main({ classes, children, roomsStore, userStore }) {
    const [ drawer, setDrawer ] = useState(true);

    useEffect(() => {
        userStore.checkPermission();
        roomsStore.fetchRooms();
    }, []);

    function handleDrawerOpen() {
        setDrawer(true);
    }
    function handleDrawerClose() {
        setDrawer(false);
    }

    return (
        <div className={classes.root}>
            <CssBaseline />
            <ApplicationBar open={drawer} handleDrawerOpen={handleDrawerOpen} />
            <MenuBar open={drawer} handleDrawerClose={handleDrawerClose} />
            <main className={classes.content}>
                <div className={classes.appBarSpacer} />
                {children}
            </main>
        </div>
    );
}

Main.propTypes = {
    classes    : PropTypes.object.isRequired,
    children   : PropTypes.node.isRequired,
    userStore  : PropTypes.object.isRequired,
    roomsStore : PropTypes.object.isRequired
};

export default inject(
    "userStore",
    "roomsStore"
)(observer(withStyles(muiStyles)(Main)));
