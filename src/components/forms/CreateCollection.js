import React, { useState } from "react";
import PropTypes from "prop-types";
import { observer, inject } from "mobx-react";

import { Button } from "@material-ui/core";
import withStyles from "@material-ui/core/styles/withStyles";

import InputField from "../elements/InputField";
import { muiStyles } from "./signInRegister.styles";

function CreateCollection({ classes, onClose, collectionStore, roomId }) {
    const [ errors ] = useState({});
    const [ name, setName ] = useState("name");

    function handleChangeName(evt) {
        setName(evt.target.value);
    }

    async function handleSubmit(evt) {
        evt.preventDefault();
        evt.stopPropagation();

        await collectionStore.createCollection(roomId, { name });
    }

    return (
        <form onSubmit={handleSubmit}>
            <InputField
                id="input-name"
                elementStyle={classes.formControl}
                value={name}
                label="Name"
                error={errors && errors.name}
                handleChange={handleChangeName}
                required
                autoFocus
                fullWidth
            />
            <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
            >
                Create room
            </Button>
            <Button
                type="button"
                fullWidth
                onClick={onClose}
                variant="contained"
                className={classes.submit}
            >
                Cancel
            </Button>
        </form>
    );
}

CreateCollection.propTypes = {
    classes         : PropTypes.object.isRequired,
    collectionStore : PropTypes.object.isRequired,
    onClose         : PropTypes.func.isRequired,
    roomId          : PropTypes.func.isRequired
};

export default inject("collectionStore")(
    observer(withStyles(muiStyles)(CreateCollection))
);
