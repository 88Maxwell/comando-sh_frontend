import React, { useState } from "react";
import PropTypes from "prop-types";
import { observer, inject } from "mobx-react";

import { Button } from "@material-ui/core";
import withStyles from "@material-ui/core/styles/withStyles";

import InputField from "../elements/InputField";
import Select from "../elements/Select";
import { muiStyles } from "./signInRegister.styles";

function CreateScript({
    collectionStore,
    collectionId,
    executors,
    classes,
    onClose
}) {
    const { data, setData } = useState("");
    const { executor, setExecutor } = useState("");
    const { errors } = useState({});

    function handleChange(setter) {
        return event => setter(event.target.value);
    }
    async function handleSubmit(evt) {
        evt.preventDefault();
        evt.stopPropagation();
        const executorId = executors.find(el => el.name === executor).id;

        await collectionStore.addScript(collectionId, { data, executorId });
    }

    return (
        <form onSubmit={handleSubmit}>
            <InputField
                id="input-data"
                elementStyle={classes.formControl}
                value={data}
                label="Script"
                error={errors && errors.data}
                handleChange={handleChange(setData)}
                required
                autoFocus
                fullWidth
            />
            <Select
                id="select-executor"
                elementStyle={classes.formControl}
                required
                label="Executor"
                error={errors && errors.executor}
                value={executor}
                valueList={executors.map(el => el.name)}
                handleChange={handleChange(setExecutor)}
            />
            <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
            >
                Create script
            </Button>
            <Button
                type="button"
                fullWidth
                onClick={onClose}
                variant="contained"
                className={classes.submit}
            >
                Cancel
            </Button>
        </form>
    );
}

CreateScript.propTypes = {
    classes         : PropTypes.object.isRequired,
    collectionStore : PropTypes.object.isRequired,
    collectionId    : PropTypes.string.isRequired,
    onClose         : PropTypes.func.isRequired,
    executors       : PropTypes.array
};

export default inject("collectionStore")(
    observer(withStyles(muiStyles)(CreateScript))
);
