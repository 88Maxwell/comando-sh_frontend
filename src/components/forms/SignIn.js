import React, { useState } from "react";
import PropTypes from "prop-types";
import { Button } from "@material-ui/core";
import { observer, inject } from "mobx-react";

import withStyles from "@material-ui/core/styles/withStyles";
import InputField from "../elements/InputField";
import { muiStyles } from "./signInRegister.styles";

function SignIn({ classes, userStore }) {
    const [ email, setEmail ] = useState("");
    const [ password, setPassword ] = useState("");
    const [ errors ] = useState({});

    async function handleSubmit(evt) {
        evt.preventDefault();
        evt.stopPropagation();

        await userStore.signIn({ email, password });
    }

    function handleChange(setter) {
        return (event) => setter(event.target.value);
    }

    return (
        <form onSubmit={handleSubmit} className={classes.form}>
            <InputField
                id="input-email"
                elementStyle={classes.formControl}
                value={email}
                type="email"
                label="Email address"
                error={errors && errors.email}
                handleChange={handleChange(setEmail)}
                autoComplete="email"
                required
                autoFocus
                fullWidth
            />
            <InputField
                id="input-password"
                elementStyle={classes.formControl}
                value={password}
                type="password"
                label="Password"
                error={errors && errors.password}
                handleChange={handleChange(setPassword)}
                autoComplete="current-password"
                required
                autoFocus
                fullWidth
            />
            <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
            >
                Sign in
            </Button>
        </form>
    );
}

SignIn.propTypes = {
    classes   : PropTypes.object.isRequired,
    userStore : PropTypes.object.isRequired
};

export default inject("userStore")(observer(withStyles(muiStyles)(SignIn)));
