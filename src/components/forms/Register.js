
import React, { useState } from "react";
import PropTypes from "prop-types";
import { observer, inject } from "mobx-react";

import { Button } from "@material-ui/core";
import withStyles from "@material-ui/core/styles/withStyles";

import InputField from "../elements/InputField";
import { muiStyles } from "./signInRegister.styles";

function Register({ classes, userStore }) {
    const [ errors ] = useState({});
    const [ email, setEmail ] = useState("");
    const [ password, setPassword ] = useState("");
    const [ password2, setPassword2 ] = useState("");

    function handleChange(setter) {
        return (event) => setter(event.target.value);
    }

    async function handleSubmit(evt) {
        evt.preventDefault();
        evt.stopPropagation();
        const {  } = this.props;

        await userStore.register({ email, password });
    }

    return (
        <form onSubmit={handleSubmit} className={classes.form}>
            <InputField
                id="input-email"
                elementStyle={classes.formControl}
                value={email}
                type="email"
                label="Email address"
                error={errors && errors.email}
                handleChange={handleChange(setEmail)}
                autoComplete="email"
                required
                autoFocus
                fullWidth
            />
            <InputField
                id="input-password"
                elementStyle={classes.formControl}
                value={password}
                type="password"
                label="Password"
                error={errors && errors.password}
                handleChange={handleChange(setPassword)}
                autoComplete="current-password"
                required
                autoFocus
                fullWidth
            />
            <InputField
                id="input-password2"
                elementStyle={classes.formControl}
                value={password2}
                type="password"
                label="Verify password"
                error={errors && errors.password}
                handleChange={handleChange(setPassword2)}
                autoComplete="current-password"
                required
                autoFocus
                fullWidth
            />
            <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
            >
                Register
            </Button>
        </form>
    );
}

Register.propTypes = {
    classes   : PropTypes.object.isRequired,
    userStore : PropTypes.object.isRequired
};


export default inject("userStore")(observer(withStyles(muiStyles)(Register)));
