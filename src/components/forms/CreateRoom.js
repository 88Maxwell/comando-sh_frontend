
import React, { useState } from "react";
import PropTypes from "prop-types";
import { observer, inject } from "mobx-react";

import { Button } from "@material-ui/core";
import withStyles from "@material-ui/core/styles/withStyles";

import InputField from "../elements/InputField";
import { muiStyles } from "./signInRegister.styles";

function CreateRoom({ classes, onClose, roomsStore }) {
    const [ errors ] = useState({});
    const [ name, setName ] = useState("");
    const [ password, setPassword ] = useState("");
    const [ password2, setPassword2 ] = useState("");

    function handleChange(setter) {
        return (event) => setter(event.target.value);
    }
    async function handleSubmit(evt) {
        evt.preventDefault();
        evt.stopPropagation();

        await roomsStore.createRoom({ name, password });
    }

    return (
        <form onSubmit={handleSubmit}>
            <InputField
                id="input-name"
                elementStyle={classes.formControl}
                value={name}
                label="Name"
                error={errors && errors.name}
                handleChange={handleChange(setName)}
                required
                autoFocus
                fullWidth
            />
            <InputField
                id="input-password"
                elementStyle={classes.formControl}
                value={password}
                type="password"
                label="Password"
                error={errors && errors.password}
                handleChange={handleChange(setPassword)}
                autoComplete="current-password"
                required
                autoFocus
                fullWidth
            />
            <InputField
                id="input-password2"
                elementStyle={classes.formControl}
                value={password2}
                type="password"
                label="Verify password"
                error={errors && errors.password}
                handleChange={handleChange(setPassword2)}
                autoComplete="current-password"
                required
                autoFocus
                fullWidth
            />
            <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
            >
                Create room
            </Button>
            <Button
                type="button"
                fullWidth
                onClick={onClose}
                variant="contained"
                className={classes.submit}
            >
                Cancel
            </Button>
        </form>
    );
}

CreateRoom.propTypes = {
    classes    : PropTypes.object.isRequired,
    roomsStore : PropTypes.object.isRequired,
    onClose    : PropTypes.func.isRequired
};

export default inject("roomsStore")(
    observer(withStyles(muiStyles)(CreateRoom))
);
