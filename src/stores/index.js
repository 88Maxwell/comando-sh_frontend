import UserStore from "./User";
import RoomsStore from "./Rooms";
import CollectionStore from "./Collection";

class Store {
    constructor() {
        this.userStore = new UserStore(this);
        this.roomsStore = new RoomsStore(this);
        this.collectionStore = new CollectionStore(this);
    }
}

export default new Store();
