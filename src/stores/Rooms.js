import { action, observable, decorate } from "mobx";

import { rooms as roomsApi } from "../api";
// import { openConnection, closeConnection } from "../socketApi";
import history from "../utils/history";

class RoomsStore {
    rooms = [];

    constructor({ userStore }) {
        this.connection = false;
        this.userStore = userStore;
    }

    deleteRoom = async id => {
        const { user } =  this.userStore;
        const deletedRoom = await roomsApi.delete(user.id, id);

        if (deletedRoom) {
            const index = this.rooms.findIndex(room => room.id === id);

            this.rooms.splice(index);
            history.push("/rooms");
            this.setConnection();
        }
    }

    createRoom = async data => {
        const { user } =  this.userStore;
        const room = await roomsApi.create(user.id, data);

        this.rooms.unshift(room);
    }

    fetchRoom = async roomId => {
        const { user } =  this.userStore;
        const data = await roomsApi.show(user.id, roomId);

        return data;
    }

    changeStatus = async (id, status) => {
        const { user } =  this.userStore;
        const updatedRoom = await roomsApi.update(user.id, id, { status });
        const index = this.rooms.findIndex(room => room.id === id);

        this.rooms[index] = updatedRoom;
    }

    fetchRooms = async () => {
        const { user } =  this.userStore;
        const rooms = await roomsApi.list(user.id);

        this.rooms = rooms ? rooms : [];
    }
}


export default decorate(RoomsStore, {
    rooms        : observable,
    deleteRoom   : action,
    createRoom   : action,
    changeStatus : action,
    fetchRoom    : action,
    fetchRooms   : action
});
