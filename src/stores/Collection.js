import { decorate, action, observable } from "mobx";

import { collection as collectionApi, script as scriptApi } from "../api";

class CollectionStore {
    collections = [];

    constructor({ roomsStore }) {
        this.roomsStore = roomsStore;
    }

    createCollection = async (id, data) => {
        const collection = await collectionApi.create(id, data);

        this.collections.unshift(collection);
    };

    fetchCollections = async id => {
        this.collections = await collectionApi.list(id);
    };

    addScript = (collectionId, data) => {
        const collection = this.findById(this.collections, collectionId);

        if (!collection.Scripts) {
            collection.Scripts = [];
        }

        collection.Scripts.push(data);
    };

    executeCollection = async (collectionId, scriptsData) => {
        await Promise.all(scriptsData.map((scriptData, index) =>
            scriptApi.create(collectionId, scriptData.executorId, { ...scriptData, number: index }))
        );
    };

    findById = (array, id) => array.find(el => el.id === id);
}

export default decorate(CollectionStore, {
    collections       : observable,
    createCollection  : action,
    fetchCollections  : action,
    addScript         : action,
    executeCollection : action
});
