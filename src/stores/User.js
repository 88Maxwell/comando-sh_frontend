import { action, decorate } from "mobx";
import jwt                  from "jwt-simple";
import request              from "../utils/request";
import history              from "../utils/history";

import { user as userApi, session as sessionApi } from "../api";

class UserStore {
  user = {};

  register = data => this.auth(false, data);

  signIn = data => this.auth(true, data);

  logout = () => {
      localStorage.clear();
      request.setToken(null);
      history.push("/");
  };

  auth = async (isRegistred, data) => {
      const getToken = isRegistred ? sessionApi.create : userApi.create;
      const { error, token } = await getToken(data);

      if (error) throw error;

      if (token) {
          this.user = jwt.decode(token, "", true);
          request.setToken(token);
          localStorage.setItem("token", token);
          history.push("/profile");
      }
  };

  checkPermission = () => {
      const { token } = localStorage;

      if (!token) return history.push("/");
      if (objIsEmpty(this.user)) {
          this.user = jwt.decode(token, "", true);
      }
  };
}

function objIsEmpty(obj) {
    return Object.entries(obj).length === 0 && obj.constructor === Object;
}

export default decorate(UserStore, {
    register : action,
    signIn   : action
});
