import request from "./utils/request";

export const user = {
    create : data => request.post("/user", data)
};

export const session = {
    create : data => request.post("/session", data)
};

export const rooms = {
    create : (userId, data) => request.post(`/user/${userId}/room`, data),
    update : (userId, roomId, data) => request.patch(`/user/${userId}/room/${roomId}`, data),
    list   : userId => request.get(`/user/${userId}/rooms`),
    show   : (userId, roomId) => request.get(`/user/${userId}/room/${roomId}`),
    delete : (userId, roomId) => request.delete(`/user/${userId}/room/${roomId}`)
};

export const collection = {
    create : roomId => request.post(`/user/room/${roomId}/collection`),
    list   : roomId => request.get(`/user/room/${roomId}/collections`),
    delete : (roomId, collectionId) => request.delete(`/user/room/${roomId}/collection/${collectionId}`),
    update : (roomId, collectionId) => request.patch(`/user/room/${roomId}/collection/${collectionId}`)
};

export const script = {
    create : (collectionId, executorId, data) => request.post(`/user/room/collection/${collectionId}/executor/${executorId}/script`, data)
};
