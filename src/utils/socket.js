import openSocket from "socket.io-client";
import { apiUrl } from "../../configs";

export default openSocket(apiUrl);
