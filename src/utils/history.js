import { createBrowserHistory } from "history";
import qhistory             from "qhistory";
import { parse, stringify } from "query-string";

const history = createBrowserHistory();

export default qhistory(history, stringify, parse);
