/* eslint-disable react/jsx-no-bind */
import React from "react";
import { Route } from "react-router";
import PropTypes from "prop-types";

const Controller = ({ Page, Layout, ...rest }) => (
    <Route
        {...rest}
        render={props => <Layout>{Page && <Page {...props} />}</Layout>}
    />
);

Controller.propTypes = {
    Layout : PropTypes.object.isRequired,
    Page   : PropTypes.any
};

export default Controller;
