import config from "../../configs";

class API {
    constructor({ prefix = "api/v1" } = {}) {
        this.prefix = prefix;
        this.token  = "";
        [ "get", "post", "patch", "delete" ].forEach(method =>
            this[method] = async (url, data) =>
                this.request({
                    url,
                    method : method.toUpperCase(),
                    body   : { data }
                }));
    }

    async request({ url, method, body }) {
        const response = await fetch(`${config.apiUrl}/${this.prefix}${url}`, {
            method,
            headers : {
                "Content-Type"  : "application/json",
                "Cache-Control" : "no-cache",
                "pragma"        : "no-cache",
                ...(this.token ? { "X-AuthToken": this.token } : {})
            },
            withCredentials : true,
            crossDomain     : false,
            body            : method !== "GET" ? JSON.stringify(body) : undefined
        });

        const { data, status, error } = await response.json();

        if (status !== 200) throw error;

        return data;
    }

    setToken(token) {
        this.token = token;
    }
}

export default new API({ prefix: config.apiPrefix });
